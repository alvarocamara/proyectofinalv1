<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "telefonos".
 *
 * @property int $codigotelefonos
 * @property string|null $telefonos
 * @property int|null $codigoclientef
 *
 * @property Clientes $codigoclientef0
 */
class telefonos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'telefonos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigoclientef'], 'integer'],
            [['telefonos'], 'string', 'max' => 9],
            [['codigoclientef', 'telefonos'], 'unique', 'targetAttribute' => ['codigoclientef', 'telefonos']],
            [['codigoclientef'], 'exist', 'skipOnError' => true, 'targetClass' => Clientes::className(), 'targetAttribute' => ['codigoclientef' => 'codigocliente']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigotelefonos' => 'Código de teléfono',
            'telefonos' => 'Teléfono',
            'codigoclientef' => 'Código cliente',
        ];
    }

    /**
     * Gets query for [[Codigoclientef0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoclientef0()
    {
        return $this->hasOne(Clientes::className(), ['codigocliente' => 'codigoclientef']);
    }
   
    public function getdropdownclientes(){
    $vtlf = Clientes::find()->asArray()->all();
    
    return \yii\helpers\ArrayHelper::map($vtlf,'telefonos', 'telefonos', 'nombre', 'apellidos');
}
}



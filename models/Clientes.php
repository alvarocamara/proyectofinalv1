<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "clientes".
 *
 * @property int $codigocliente
 * @property string|null $nombre
 * @property string|null $direccion
 * @property string|null $apellidos
 * @property string|null $telefonos
 * @property int|null $subscripcion
 *
 * @property Compras[] $compras
 * @property Telefonos[] $telefonos0
 */
class Clientes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clientes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['subscripcion'], 'integer'],
            [['nombre'], 'string', 'max' => 20],
            [['direccion'], 'string', 'max' => 50],
            [['apellidos', 'telefonos'], 'string', 'max' => 25],
            [['telefonos'], 'k-phone', 'countryValue' => 'ES'],

        ];
    }
    


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigocliente' => 'Codigocliente',
            'nombre' => 'Nombre',
            'direccion' => 'Direccion',
            'apellidos' => 'Apellidos',
            'telefonos' => 'Teléfono',
            'subscripcion' => 'Subscripcion',
        ];
    }

    /**
     * Gets query for [[Compras]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompras()
    {
        return $this->hasMany(Compras::className(), ['codigoclientef2' => 'codigocliente']);
    }

    /**
     * Gets query for [[Telefonos0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTelefonos0()
    {
        return $this->hasMany(Telefonos::className(), ['codigoclientef' => 'codigocliente']);
    }
}



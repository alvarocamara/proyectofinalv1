<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "productos".
 *
 * @property int $codigoproductos
 * @property string|null $nombre
 * @property float|null $precioventa
 * @property int|null $codigoproveedorf
 * @property int|null $codigotiposf
 * @property int|null $cantidad
 * @property int|null $cantidadmin
 *
 * @property Proveedores $codigoproveedorf0
 * @property Tipos $codigotiposf0
 * @property Compras[] $compras
 */
class productos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'productos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['precioventa'], 'number'],
            [['codigoproveedorf', 'codigotiposf', 'cantidad', 'cantidadmin'], 'integer'],
            [['nombre'], 'string', 'max' => 25],
            [['codigoproveedorf'], 'exist', 'skipOnError' => true, 'targetClass' => Proveedores::className(), 'targetAttribute' => ['codigoproveedorf' => 'codigoproveedores']],
            [['codigotiposf'], 'exist', 'skipOnError' => true, 'targetClass' => Tipos::className(), 'targetAttribute' => ['codigotiposf' => 'codigotipos']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
       return [
            'codigoproductos' => 'Codigo de producto',
            'nombre' => 'Nombre',
            'precioventa' => 'Precio de Venta',
            'codigoproveedorf' => 'Proveedor',
            'codigotiposf' => 'Categoria',
            'cantidad' => 'Cantidad',
            'cantidadmin' => 'Cantidad Minima de Stock',
            'codigotiposf0' => 'Categoria'
        ];

    }

    /**
     * Gets query for [[Codigoproveedorf0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoproveedorf0()
    {
        return $this->hasOne(Proveedores::className(), ['codigoproveedores' => 'codigoproveedorf']);
    }

    /**
     * Gets query for [[Codigotiposf0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigotiposf0()
    {
        return $this->hasOne(Tipos::className(), ['codigotipos' => 'codigotiposf']);
    }

    /**
     * Gets query for [[Compras]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompras()
    {
        return $this->hasMany(Compras::className(), ['codigoproductosf' => 'codigoproductos']);
    }
    
     public function getdropdowntipos(){
    $vtipos = Tipos::find()->asArray()->all();
    
    return \yii\helpers\ArrayHelper::map($vtipos,'codigotipos', 'nombre');
}

    public function getdropdownproveedores(){
    $vproveedores = Proveedores::find()->asArray()->all();
    
    return \yii\helpers\ArrayHelper::map($vproveedores,'codigoproveedores', 'nombre', 'producto');
}
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "embajadores".
 *
 * @property int $codigoembajador
 * @property string|null $nombre
 * @property float|null $numeroventas
 * @property int|null $numeroregistros
 * @property string|null $telefono
 *
 * @property Compras[] $compras
 */
class embajadores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'embajadores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['numeroventas'], 'number'],
            [['numeroregistros'], 'integer'],
            [['nombre'], 'string', 'max' => 20],
            [['telefono'], 'k-phone', 'countryValue' => 'ES', ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigoembajador' => 'Código de embajador',
            'nombre' => 'Nombre',
            'numeroventas' => 'Número de ventas',
            'numeroregistros' => 'Número de registros',
            'telefono' => 'Teléfono',
        ];
    }

    /**
     * Gets query for [[Compras]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompras()
    {
        return $this->hasMany(Compras::className(), ['codigoembajadorf' => 'codigoembajador']);
    }
}

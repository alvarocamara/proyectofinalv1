<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "compras".
 *
 * @property int $codigocompras
 * @property string|null $producto
 * @property string|null $fecha
 * @property int|null $codigoclientef2
 * @property int|null $codigoembajadorf
 * @property int|null $codigoproductosf
 *
 * @property Clientes $codigoclientef20
 * @property Embajadores $codigoembajadorf0
 * @property Productos $codigoproductosf0
 */
class Compras extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'compras';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha'], 'safe'],
            [['codigoclientef2', 'codigoembajadorf', 'codigoproductosf'], 'integer'],
            [['producto'], 'string', 'max' => 50],
            [['codigoclientef2'], 'exist', 'skipOnError' => true, 'targetClass' => Clientes::className(), 'targetAttribute' => ['codigoclientef2' => 'codigocliente']],
            [['codigoembajadorf'], 'exist', 'skipOnError' => true, 'targetClass' => Embajadores::className(), 'targetAttribute' => ['codigoembajadorf' => 'codigoembajador']],
            [['codigoproductosf'], 'exist', 'skipOnError' => true, 'targetClass' => Productos::className(), 'targetAttribute' => ['codigoproductosf' => 'codigoproductos']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigocompras' => 'Codigocompras',
            'producto' => 'Producto',
            'fecha' => 'Fecha (YYYY-MM-DD)',
            'codigoclientef2' => 'Cliente',
            'codigoembajadorf' => 'Embajador',
            'codigoproductosf' => 'Producto',
        ];
    }

    /**
     * Gets query for [[Codigoclientef20]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoclientef20()
    {
        return $this->hasOne(Clientes::className(), ['codigocliente' => 'codigoclientef2']);
    }

    /**
     * Gets query for [[Codigoembajadorf0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoembajadorf0()
    {
        return $this->hasOne(Embajadores::className(), ['codigoembajador' => 'codigoembajadorf']);
    }

    /**
     * Gets query for [[Codigoproductosf0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoproductosf0()
    {
        return $this->hasOne(Productos::className(), ['codigoproductos' => 'codigoproductosf']);
    }
    
      
    public function getdropdownEmbajadores(){
    $vembajadores = Embajadores::find()->asArray()->all();
    
    return \yii\helpers\ArrayHelper::map($vembajadores,'codigoembajador', 'nombre');
}

    public function getdropdownProductos(){
    $vproductos = Productos::find()->asArray()->all();
    
    return \yii\helpers\ArrayHelper::map($vproductos,'codigoproductos', 'nombre');
}

  public function getdropdownClientes(){
    $vclientes = Clientes::find()->asArray()->all();
    
    return \yii\helpers\ArrayHelper::map($vclientes,'codigocliente', 'nombre','telefonos');
}
}

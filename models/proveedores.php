<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "proveedores".
 *
 * @property int $codigoproveedores
 * @property string|null $telefono
 * @property string|null $email
 * @property string|null $nombre
 * @property string|null $producto
 * @property int|null $precio
 *
 * @property Productos[] $productos
 */
class proveedores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'proveedores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['precio'], 'integer'],
            [['telefono'], 'k-phone', 'countryValue' => 'ES'],
            [['email'], 'string', 'max' => 100],
            [['nombre', 'producto'], 'string', 'max' => 25],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigoproveedores' => 'Código',
            'telefono' => 'Teléfono',
            'email' => 'E-mail',
            'nombre' => 'Nombre',
            'producto' => 'Producto',
            'precio' => 'Precio',
        ];
    }

    /**
     * Gets query for [[Productos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductos()
    {
        return $this->hasMany(Productos::className(), ['codigoproveedorf' => 'codigoproveedores']);
    }
}

<?php

namespace app\controllers;

use app\models\proveedores;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ProveedoresController implements the CRUD actions for proveedores model.
 */
class ProveedoresController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all proveedores models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => proveedores::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'codigoproveedores' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single proveedores model.
     * @param int $codigoproveedores Codigoproveedores
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codigoproveedores)
    {
        return $this->render('view', [
            'model' => $this->findModel($codigoproveedores),
        ]);
    }

    /**
     * Creates a new proveedores model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new proveedores();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'codigoproveedores' => $model->codigoproveedores]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing proveedores model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $codigoproveedores Codigoproveedores
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codigoproveedores)
    {
        $model = $this->findModel($codigoproveedores);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigoproveedores' => $model->codigoproveedores]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing proveedores model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $codigoproveedores Codigoproveedores
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codigoproveedores)
    {
        $this->findModel($codigoproveedores)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the proveedores model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $codigoproveedores Codigoproveedores
     * @return proveedores the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codigoproveedores)
    {
        if (($model = proveedores::findOne(['codigoproveedores' => $codigoproveedores])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

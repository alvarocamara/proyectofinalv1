<?php

namespace app\controllers;

use app\models\clientes;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ClientesController implements the CRUD actions for clientes model.
 */
class ClientesController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all clientes models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => clientes::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'codigocliente' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single clientes model.
     * @param int $codigocliente Codigocliente
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codigocliente)
    {
        return $this->render('view', [
            'model' => $this->findModel($codigocliente),
        ]);
    }

    /**
     * Creates a new clientes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new clientes();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'codigocliente' => $model->codigocliente]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing clientes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $codigocliente Codigocliente
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codigocliente)
    {
        $model = $this->findModel($codigocliente);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigocliente' => $model->codigocliente]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing clientes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $codigocliente Codigocliente
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codigocliente)
    {
        $this->findModel($codigocliente)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the clientes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $codigocliente Codigocliente
     * @return clientes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codigocliente)
    {
        if (($model = clientes::findOne(['codigocliente' => $codigocliente])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

<?php

namespace app\controllers;

use app\models\telefonos;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TelefonosController implements the CRUD actions for telefonos model.
 */
class TelefonosController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all telefonos models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => telefonos::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'codigotelefonos' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single telefonos model.
     * @param int $codigotelefonos Codigotelefonos
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codigotelefonos)
    {
        return $this->render('view', [
            'model' => $this->findModel($codigotelefonos),
        ]);
    }

    /**
     * Creates a new telefonos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new telefonos();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'codigotelefonos' => $model->codigotelefonos]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing telefonos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $codigotelefonos Codigotelefonos
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codigotelefonos)
    {
        $model = $this->findModel($codigotelefonos);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigotelefonos' => $model->codigotelefonos]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing telefonos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $codigotelefonos Codigotelefonos
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codigotelefonos)
    {
        $this->findModel($codigotelefonos)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the telefonos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $codigotelefonos Codigotelefonos
     * @return telefonos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codigotelefonos)
    {
        if (($model = telefonos::findOne(['codigotelefonos' => $codigotelefonos])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

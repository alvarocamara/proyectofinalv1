<?php

namespace app\controllers;

use app\models\tipos;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TiposController implements the CRUD actions for tipos model.
 */
class TiposController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all tipos models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => tipos::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'codigotipos' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single tipos model.
     * @param int $codigotipos Codigotipos
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codigotipos)
    {
        return $this->render('view', [
            'model' => $this->findModel($codigotipos),
        ]);
    }

    /**
     * Creates a new tipos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new tipos();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'codigotipos' => $model->codigotipos]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing tipos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $codigotipos Codigotipos
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codigotipos)
    {
        $model = $this->findModel($codigotipos);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigotipos' => $model->codigotipos]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing tipos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $codigotipos Codigotipos
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codigotipos)
    {
        $this->findModel($codigotipos)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the tipos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $codigotipos Codigotipos
     * @return tipos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codigotipos)
    {
        if (($model = tipos::findOne(['codigotipos' => $codigotipos])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

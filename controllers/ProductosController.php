<?php

namespace app\controllers;

use app\models\productos;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ProductosController implements the CRUD actions for productos model.
 */
class ProductosController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all productos models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => productos::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'codigoproductos' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single productos model.
     * @param int $codigoproductos Codigoproductos
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codigoproductos)
    {
        return $this->render('view', [
            'model' => $this->findModel($codigoproductos),
        ]);
    }

    /**
     * Creates a new productos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new productos();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'codigoproductos' => $model->codigoproductos]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing productos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $codigoproductos Codigoproductos
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codigoproductos)
    {
        $model = $this->findModel($codigoproductos);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigoproductos' => $model->codigoproductos]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing productos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $codigoproductos Codigoproductos
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codigoproductos)
    {
        $this->findModel($codigoproductos)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the productos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $codigoproductos Codigoproductos
     * @return productos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codigoproductos)
    {
        if (($model = productos::findOne(['codigoproductos' => $codigoproductos])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

<?php

namespace app\controllers;

use app\models\compras;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ComprasController implements the CRUD actions for compras model.
 */
class ComprasController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all compras models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => compras::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'codigocompras' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single compras model.
     * @param int $codigocompras Codigocompras
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codigocompras)
    {
        return $this->render('view', [
            'model' => $this->findModel($codigocompras),
        ]);
    }

    /**
     * Creates a new compras model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new compras();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'codigocompras' => $model->codigocompras]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing compras model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $codigocompras Codigocompras
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codigocompras)
    {
        $model = $this->findModel($codigocompras);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigocompras' => $model->codigocompras]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing compras model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $codigocompras Codigocompras
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codigocompras)
    {
        $this->findModel($codigocompras)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the compras model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $codigocompras Codigocompras
     * @return compras the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codigocompras)
    {
        if (($model = compras::findOne(['codigocompras' => $codigocompras])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

<?php

namespace app\controllers;

use app\models\embajadores;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * EmbajadoresController implements the CRUD actions for embajadores model.
 */
class EmbajadoresController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all embajadores models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => embajadores::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'codigoembajador' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single embajadores model.
     * @param int $codigoembajador Codigoembajador
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codigoembajador)
    {
        return $this->render('view', [
            'model' => $this->findModel($codigoembajador),
        ]);
    }

    /**
     * Creates a new embajadores model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new embajadores();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'codigoembajador' => $model->codigoembajador]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing embajadores model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $codigoembajador Codigoembajador
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codigoembajador)
    {
        $model = $this->findModel($codigoembajador);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigoembajador' => $model->codigoembajador]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing embajadores model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $codigoembajador Codigoembajador
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codigoembajador)
    {
        $this->findModel($codigoembajador)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the embajadores model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $codigoembajador Codigoembajador
     * @return embajadores the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codigoembajador)
    {
        if (($model = embajadores::findOne(['codigoembajador' => $codigoembajador])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

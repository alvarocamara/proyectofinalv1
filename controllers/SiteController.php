<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\ActiveDataProvider;
use app\models\productos;
use kartik\alert\Alert;
use yii\db\Expression;
use app\models\Compras;
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    
    

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {        
     
         return $this->render('index');     
    }
   
     public function actionReglasstock2(){
        
        
        $numero = Yii::$app->db
                ->createCommand("SELECT cantidad FROM productos WHERE 'nombre' = 'paco'")
                ->queryScalar();
        
        $cantidadmin = Yii::$app->db
                ->createCommand("SELECT cantidadmin FROM productos WHERE 'nombre' = 'paco'")
                ->queryScalar();
          
         if (($numero) <$cantidadmin){
         echo 
         $this->render('alertas');

      } else{
            return $this->render('index');     
      }
    
    }
    
      public function actionReglasstock3(){
        
         $cantidadmin = Yii::$app->db
                ->createCommand("SELECT COUNT(*) FROM productos WHERE cantidad < cantidadmin")
                ->queryScalar();
          
         if ($cantidadmin > 0) { 
          
          
      $dataProvider = new ActiveDataProvider([
            'query'=> Productos::find()
                   ->select("nombre, codigoproveedorf")
                   ->distinct()
                   ->where("cantidad < cantidadmin"),
                   
        ]);
        
        return $this->render("alertas",[
            "resultados"=>$dataProvider,
            "campos"=>['Nombre, Proveedor '],
            "titulo"=>"Nos falta Stock en estos productos",
            "enunciado"=>"Consulta con los proveedores",
            
        ]);
         } else {
           return $this->render('nostock');  
         }
    }
    
    

     public function actionManual()
    {
        return $this->render('manual');
        
    }
    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
     public function actionGestion()
    {
        return $this->render('gestion');
    }
    
    public function actionCdd()
    {
        $grafCategoria = $this->grafCategoria();
        $ventasPormes = $this->ventasPormes();
        $grafEmbajadores = $this->grafEmbajadores();
         return $this->render('cdd',[
            'grafCategoria'=>$grafCategoria,
            'ventasPormes'=>$ventasPormes,
            'grafEmbajadores'=>$grafEmbajadores,           
        ]);

    }
    
    public function ventasPormes(){
        $expresion = new Expression("select COUNT(*) as cantidad, elt(MONTH(fecha),'Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre') as mes, year(fecha) as year from compras group by mes, year order by fecha asc limit 12");
        $query = Yii::$app->db->createCommand($expresion)->queryAll();
        return $query;
    }
    
     public function grafEmbajadores(){
        $expresion = new Expression("select numeroventas, numeroregistros, nombre as nombre2 from embajadores");
        $query = Yii::$app->db->createCommand($expresion)->queryAll();
        return $query;
    }
    
    
    
    public function grafCategoria(){
        $expresion = new Expression("select COUNT(codigoproductosf) cantidad2, nombre from compras LEFT JOIN productos ON compras.codigoproductosf = productos.codigoproductos GROUP BY codigoproductosf order by cantidad2 desc");
        $query = Yii::$app->db->createCommand($expresion)->queryAll();
        return $query;
    }


    
    
    
}
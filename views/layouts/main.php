<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\AppAsset;
use app\widgets\Alert;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use app\models\clientes;
use kartik\icons\Icon;
use kartik\icons;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">
<head>
    <link rel="shortcut icon" href="<?php echo Yii::$app->request->baseUrl; ?>/images/logo/logosinletrasnegro.png" type="image/x-icon" />
    
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="d-flex flex-column h-100">
<?php $this->beginBody() ?>

<header>
    <?php
    NavBar::begin([
        'brandLabel' => Html::img('@web/images/logo/logosinletras.png',[ 'style' => 'height: 55px'],
                    ['alt' => 'Imagen no encontrada'], ['class' => 'navbar-brand']),
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar align-items-center navbar-expand-md navbar-dark fixed-top colornavbar navbar-right',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav ' ],
        'items' => [
          // ['label' => 'Home', 'url' => ['/site/index']],
           ['label' => 'Manual De Usuario', 'url'=>null, 'linkOptions'=>['href'=>Yii::$app->request->baseUrl .'/files/' . 'manual.pdf', 'class' => '', 'target' => '_blank']],
            ['label' => 'Soporte', 'url' => ['/site/contact']],
            
        ],
    ]);
    NavBar::end();
    ?>
</header>

<main role="main" class="flex-shrink-0 espacioarriba ">
    <div id="colorbreadcrums" class="container-fluid ">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</main>

<footer class="footer mt-auto py-3 text-muted colornavbar">
    <div class="container">
        <p class="float-left coloresletrablanca">&copy; Mainstay<?= date('Y') ?></p>
        <p class="float-right coloresletrablanca">&copy; M2022CR</p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

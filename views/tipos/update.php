<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\tipos */

$this->title = 'Editar Categorías: ' . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Tipos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nombre, 'url' => ['view', 'codigotipos' => $model->codigotipos]];
$this->params['breadcrumbs'][] = 'Editar';
?>
<div class="tipos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

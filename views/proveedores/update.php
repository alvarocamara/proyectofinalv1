<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\proveedores */

$this->title = 'Editar Proveedor: ' . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Proveedores', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nombre, 'url' => ['view', 'codigoproveedores' => $model->codigoproveedores]];
$this->params['breadcrumbs'][] = 'Editar';
?>
<div class="proveedores-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\proveedores */

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Proveedores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="proveedores-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Editar', ['update', 'codigoproveedores' => $model->codigoproveedores], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Eliminar', ['delete', 'codigoproveedores' => $model->codigoproveedores], [
            'class' => 'btn btn-success',
            'data' => [
                'confirm' => '¿Seguro que desea eliminar este elemento?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'nombre',
            'telefono',
            'email:email',            
            'producto',
            'precio',
        ],
    ]) ?>

</div>

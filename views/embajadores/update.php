<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\embajadores */

$this->title = 'Editar Embajadores: ' . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Embajadores', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nombre, 'url' => ['view', 'codigoembajador' => $model->codigoembajador]];
$this->params['breadcrumbs'][] = 'Editar';
?>
<div class="embajadores-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\embajadores */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="embajadores-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'numeroventas')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'numeroregistros')->textInput() ?>

    <?= $form->field($model, 'telefono')->textInput(['placeholder' => 'Introduzca el Teléfono en formato ES']) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

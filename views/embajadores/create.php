<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\embajadores */

$this->title = 'Crear Embajadores';
$this->params['breadcrumbs'][] = ['label' => 'Embajadores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="embajadores-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

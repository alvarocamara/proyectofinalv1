<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\embajadores */

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Embajadores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="embajadores-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Editar', ['update', 'codigoembajador' => $model->codigoembajador], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Eliminar', ['delete', 'codigoembajador' => $model->codigoembajador], [
            'class' => 'btn btn-success',
            'data' => [
                'confirm' => '¿Seguro que desea eliminar este elemento?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'nombre',
            'numeroventas',
            'numeroregistros',
            'telefono',
        ],
    ]) ?>

</div>

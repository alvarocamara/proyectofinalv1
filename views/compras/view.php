<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\compras */

$this->title = $model->producto;
$this->params['breadcrumbs'][] = ['label' => 'Compras', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="compras-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Editar', ['update', 'codigocompras' => $model->codigocompras], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Eliminar', ['delete', 'codigocompras' => $model->codigocompras], [
            'class' => 'btn btn-success',
            'data' => [
                'confirm' => '¿Seguro que desea eliminar este elemento?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
              ['attribute' => 'productos',
            'value'=> $model->codigoproductosf0['nombre'],
               'label' => 'Producto'],
            'fecha',
           ['attribute' => 'cliente',
            'value'=> $model->codigoclientef20['nombre'],

               'label' => 'Cliente'],
            ['attribute' => 'embajadores',
            'value'=> $model->codigoembajadorf0['nombre'],
               'label' => 'Embajador'],


        ],
    ]) ?>

</div>

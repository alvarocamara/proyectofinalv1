<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\compras */

$this->title = 'Editar Compras: ' . $model->producto;
$this->params['breadcrumbs'][] = ['label' => 'Compras', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigocompras, 'url' => ['view', 'codigocompras' => $model->codigocompras]];
$this->params['breadcrumbs'][] = 'Editar';
?>
<div class="compras-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

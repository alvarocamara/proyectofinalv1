<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use kartik\icons\Icon;
use kartik\icons\FontAwesomeAsset;
FontAwesomeAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\compras */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="compras-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigoproductosf')->dropDownList($model->getdropdownProductos()) ?> 
            


    
    <?= $form->field($model, 'codigoclientef2')->dropDownList($model->getdropdownClientes()) ?> 
    
        
    <?= $form->field($model, 'fecha')->widget(DatePicker::class, [
        'name' => 'dp_2',
        'type' => DatePicker::TYPE_COMPONENT_PREPEND,
        'value' => '23-Feb-1982',
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'yyyy/mm/dd'
        ]
    ]);
?>
    <?= $form->field($model, 'codigoembajadorf')->dropDownList($model->getdropdownEmbajadores()) ?> 

 

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

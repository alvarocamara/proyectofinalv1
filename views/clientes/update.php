<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\clientes */

$this->title = 'Editar Cliente: ' . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Clientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nombre, 'url' => ['view', 'codigocliente' => $model->codigocliente]];
$this->params['breadcrumbs'][] = 'Editar';
?>
<div class="clientes-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\productos */

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Productos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="productos-view">

    

    <p>
        <?= Html::a('Editar', ['update', 'codigoproductos' => $model->codigoproductos], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Borrar', ['delete', 'codigoproductos' => $model->codigoproductos], [
            'class' => 'btn btn-success',
            'data' => [
                'confirm' => '¿Esta seguro que desea eliminar este producto?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            
            'nombre',
            'precioventa',       
           ['attribute' => 'nombre',
            'value'=> $model->codigoproveedorf0['nombre'],
               'label' => 'Proveedor'],
            ['attribute' => 'nombre',
            'value'=> $model->codigotiposf0['nombre'],
               'label' => 'Categoria'],
            'cantidad',
            'cantidadmin',
        ],
       
    ]) ?>

</div>

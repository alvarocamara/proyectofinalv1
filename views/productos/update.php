<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\productos */

$this->title = 'Editar Producto: ' . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Productos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nombre, 'url' => ['view', 'codigoproductos' => $model->codigoproductos]];
$this->params['breadcrumbs'][] = 'Editar';
?>
<div class="productos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

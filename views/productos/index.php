<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Productos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="productos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear productos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            
            'nombre',
            'precioventa',
             [
                "attribute" => 'proveedor',
                "value" => 'codigoproveedorf0.nombre'
            ],
             [
                "attribute" => 'categoria',
                "value" => 'codigotiposf0.nombre'
            ],
           
            //'tipo',
            //'cantidad',
            //'cantidadmin',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'codigoproductos' => $model->codigoproductos]);
                 }
            ],
        ],
    ]); ?>



</div>

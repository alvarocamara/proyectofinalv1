<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\productos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="productos-form">

   
 <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'precioventa')->textInput() ?>

    <?= $form->field($model, 'codigoproveedorf')->dropDownList($model->getdropdownproveedores()) ?> 

    <?= $form->field($model, 'codigotiposf')->dropDownList($model->getdropdowntipos()) ?> 

    <?= $form->field($model, 'cantidad')->textInput() ?>

    <?= $form->field($model, 'cantidadmin')->textInput() ?>


    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;
use yii\helpers\Url;
use coderius\swiperslider\SwiperSlider;
use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;
/* @var $this yii\web\View */

$this->title = 'Centro de datos';
?>
<div class="row">
    <div class="col-md-6">
 <?=
    Highcharts::widget([
    'scripts' => [
        'modules/exporting',
        'themes/grid-light',
    ],
    'options' => [
        'title' => [
            'text' => 'Trafico en Tienda Online',
        ],
        'xAxis' => [
            'categories' => ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo'],
        ],
        'labels' => [
            'items' => [
                [
                    'html' => 'Afluencia usuarios',
                    'style' => [
                        'left' => '50px',
                        'top' => '18px',
                        'color' => new JsExpression('(Highcharts.theme && Highcharts.theme.textColor) || "black"'),
                    ],
                ],
            ],
        ],
        'series' => [
            
            [
                'type' => 'spline',
                'name' => 'INGRESOS',
                'data' => [100, 150, 122, 145, 323],
                'marker' => [
                    'lineWidth' => 5,
                    'lineColor' => new JsExpression('Highcharts.getOptions().colors[3]'),
                    'fillColor' => 'white',
                ],
            ],         
        ],
    ]
]);
?>
        </div>
    <div class="col-md-6">
     <?php   
    foreach($ventasPormes as $values){
                        $mes[] = ($values['mes']);
                        $cantidad[] = intval($values['cantidad']);
                    }
                    echo
                    Highcharts::widget([
                        'scripts' => ['modules'],
                        'options' => [
                            'chart' => ['type' => 'column'],
                            'title' => ['text' => 'Ventas'],
                            'xAxis' => ['categories' => $mes],
                            'yAxis' => ['title' => ['text' => 'Cantidad']],
                            'series' => [
                                [
                                   'name' => 'Ventas',
                                   'color' => 'orange',
                                   'colorByPoint' => false,
                                   'data' => $cantidad,
                                ],
                            ],
                        ],
                    ]);
?>
        </div>
    <div class="col-md-6">
      <?php
    foreach($grafCategoria as $values){
                        $nombre[] = ($values['nombre']);
                        $cantidad2[] = intval($values['cantidad2']);
                    }
                    echo
                    Highcharts::widget([
                        'scripts' => ['modules'],
                        'options' => [
                            'chart' => ['type' => 'column'],
                            'title' => ['text' => 'Productos más vendidos'],
                            'xAxis' => ['categories' => $nombre],
                            'yAxis' => ['title' => ['text' => 'Cantidad']],
                            'series' => [
                                [
                                   'name' => 'Productos',
                                   'color' => ' #3498db ',
                                   'colorByPoint' => false,
                                    'data' => $cantidad2,
                                ],
                            ],
                        ],
                    ]);
?>
        
        </div>
    <div class="col-md-6">
      <?php
    foreach($grafEmbajadores as $values){
                        $nombre2[] = ($values['nombre2']);
                        $registros[] = intval($values['numeroregistros']);
                        $ventas[] = intval($values['numeroventas']);
                    }
                    echo
                    Highcharts::widget([
                        'scripts' => ['modules/exporting'],
                        'options' => [
                            'chart' => ['type' => 'column'],
                            'title' => ['text' => 'Rendimiento Embajadores'],
                            'xAxis' => ['categories' => $nombre2],
                            'yAxis' => ['title' => ['text' => 'Cantidad']],
                            'series' => [
                                [
                                   'name' => 'Ventas',
                                   'color' => '#FF74A6',
                                   'colorByPoint' => false,
                                   'data' => $ventas,
                                ],
                                
                                [
                                   'name' => 'Registros',
                                   'color' => '#2B908F',
                                   'colorByPoint' => false,
                                   'data' =>  $registros,
                                ],
                            ],
                        ],
                    ]);
?>
        </div>
  
    
 </div>
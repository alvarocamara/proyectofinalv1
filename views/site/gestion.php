<?php
 
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'Gestión';
?>


<div class="row">
    <div class="col-6 ">         
        <div class="card3 zoom coloresletrablanca container-fluid flex-shrink-0 ">                  
            <div class="paddfotos">                    
                <?= Html::a(Html::img('@web/images/logo/logoproductos.png', ['alt' => 'Imagen no encontrada', 'class' => ' imgcard']), ['productos/index']) ?>
            </div>
        </div>           
    </div>
    <div class="col-3">         
        <div class="card zoom paddfotos">
            <div class="">       
                <?= Html::a(Html::img('@web/images/logo/logocompras.png', ['alt' => 'Imagen no encontrada', 'class' => ' imgcard2']), ['compras/index']) ?>
            </div>
        </div>
    </div> 
    <div class="col-3">
        <div class="card zoom paddfotos">
            <div class="">                      
                <?= Html::a(Html::img('@web/images/logo/logoproveedores.png', ['alt' => 'Imagen no encontrada', 'class' => ' imgcard2']), ['proveedores/index']) ?>
            </div>
        </div>            
    </div> 
</div>
<div class="row">
    <div class="col-6 ">

        <div class="card3 zoom coloresletrablanca container-fluid flex-shrink-0 espaciocard paddfotos">

            <div class="">                  
                <?= Html::a(Html::img('@web/images/logo/logoclientes.png', ['alt' => 'Imagen no encontrada', 'class' => ' imgcard']), ['clientes/index']) ?>
            </div>
        </div>
    </div>
    <div class="col-3" style="margin-bottom: 43px;" >
        <div class="card zoom espaciocard paddfotos">
            <div class="">       
                <?= Html::a(Html::img('@web/images/logo/logoembajadores.png', ['alt' => 'Imagen no encontrada', 'class' => ' imgcard2']), ['embajadores/index']) ?>
            </div>
        </div>
    </div> 
    <div class="col-3" style="margin-bottom: 43px;">
        <div class="card zoom espaciocard paddfotos ">                
            <div class="">   
                <?= Html::a(Html::img('@web/images/logo/logocategorias.png', ['alt' => 'Imagen no encontrada', 'class' => ' imgcard2']), ['tipos/index']) ?>
            </div>
        </div>
    </div> 
</div>
</div

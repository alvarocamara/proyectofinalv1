<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
//use module\user\assets\UserAsset;
use yii\widgets\DetailView;


$this->title = 'Stock';
        ?>
 <div class="card4 flex-shrink-0 coloresletrablanca ">
             
                <h6 class="separator">
                    
                    Necesitamos Stock en estos productos:
                </h6>
 
        </div>

<div class="separator table">
    
</div>

<div class="">
    <div class="">   
 
    </div>
<?=GridView::widget([
    'dataProvider' => $resultados,
     'columns' => [ 
            ['class' => 'yii\grid\SerialColumn'],
            'nombre',
            ['class' => 'yii\grid\ActionColumn',
                'header' => 'Proveedores',
                'template' => '{proveedores}',
                'buttons' => [
                    'proveedores' => function($url, $model) {
                        return Html::a('Ir al proveedor', ['proveedores/view', 'codigoproveedores' => $model['codigoproveedorf']], ['class'=>'btn btn-success']);
                }
                ],
             ],
         ],
    'layout' => "\n{items}\n{pager}",
   // 'tableOptions' => ['class' => '']
]); ?>
</div>
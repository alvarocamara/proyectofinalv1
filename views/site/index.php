<?php

use yii\helpers\Html;
use yii\helpers\Url;
use coderius\swiperslider\SwiperSlider;
use yii\bootstrap4\Alert;
//use module\user\assets\UserAsset;
use app\controllers\SiteController;

/* @var $this yii\web\View */
//UserAsset::register(Yii::$app->view);
$this->title = 'Mainstay';
?>
<div class="row">
    <div class="col-md-6  ">
        <div class="card3 zoom container-fluid flex-shrink-0  ">
            <div class="  paddfotos">   
<?= Html::a(Html::img('@web/images/logo/logogestion.png', ['alt' => 'Imagen no encontrada', 'class' => ' imgcard2']), ['site/gestion']) ?>
            </div>
        </div>
        <div class="separator">
        </div>
        <div class="padslider">
            <?=
            SwiperSlider::widget([
                'slides' => [
                    //Html::img('@web/images/fotosmainstay/slider1mst_1.png', ['alt' => 'Imagen no encontrada', 'class' => '']),
                    Html::img('@web/images/fotosmainstay/sliderfoto1.png', ['alt' => 'Imagen no encontrada', 'class' => '']),
                    Html::img('@web/images/fotosmainstay/fotoslider3.png', ['alt' => 'Imagen no encontrada', 'class' => '']),
                    Html::img('@web/images/fotosmainstay/fotoslider4.png', ['alt' => 'Imagen no encontrada', 'class' => '']),
                    Html::img('@web/images/fotosmainstay/fotoslider5.png', ['alt' => 'Imagen no encontrada', 'class' => '']),
                ],
                'options' => [
                    'styles' => [
                        SwiperSlider::CONTAINER => ["height" => "350px", "width" => "100%"],
                        SwiperSlider::SLIDE => ["text-align" => "center"],
                    ],
                ],
            ]);
            ?>
        </div>
    </div>
    <div class="col-md-6  ">
        <div class="card3 zoom container-fluid flex-shrink-0">
            <div class=" paddfotos">   
<?= Html::a(Html::img('@web/images/logo/logocdd.png', ['alt' => 'Imagen no encontrada', 'class' => 'imgcard2']), ['site/cdd']) ?> 
            </div>
        </div>
        <div class="card3  zoom container-fluid flex-shrink-0 espaciocard ">


            <div class=" paddfotos">   
<?= Html::a(Html::img('@web/images/logo/logoads_1.png', ['alt' => 'Imagen no encontrada', 'class' => 'imgcard2']), ['site/reglasstock3']) ?> 
            </div>
        </div>
    </div>
</div>
<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\telefonos */

$this->title = 'Update Telefonos: ' . $model->codigotelefonos;
$this->params['breadcrumbs'][] = ['label' => 'Telefonos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigotelefonos, 'url' => ['view', 'codigotelefonos' => $model->codigotelefonos]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="telefonos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
